# SAT Extended Library

[Project Wiki](https://sat-polsl.gitlab.io/wiki/satext-wiki/#/)


### Credits
- [Sy Brand](https://github.com/TartanLlama) for [`tl::expected`](https://github.com/TartanLlama/expected)
- [cor3ntin](https://github.com/cor3ntin) for [`cor3ntin::rangesnext`](https://github.com/cor3ntin/rangesnext)
- [Yiftach Karkason](https://github.com/karkason) for [`pystruct`](https://github.com/karkason/cppystruct)
