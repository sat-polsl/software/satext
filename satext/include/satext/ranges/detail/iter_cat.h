#pragma once

#include <ranges>

namespace satext::ranges::detail {
template<typename... V>
consteval auto iter_cat() {
    if constexpr ((std::ranges::random_access_range<V> && ...))
        return std::random_access_iterator_tag{};
    else if constexpr ((std::ranges::bidirectional_range<V> && ...))
        return std::bidirectional_iterator_tag{};
    else if constexpr ((std::ranges::forward_range<V> && ...))
        return std::forward_iterator_tag{};
    else if constexpr ((std::ranges::input_range<V> && ...))
        return std::input_iterator_tag{};
    else
        return std::output_iterator_tag{};
}
} // namespace satext::ranges::detail
