#pragma once
#include <ranges>
#include "satext/ranges/detail/iter_cat.h"

namespace satext::ranges {

// clang-format off
template<class R>
concept simple_view = // exposition only
    std::ranges::view<R> && std::ranges::range<const R> &&
    std::same_as<std::ranges::iterator_t<R>, std::ranges::iterator_t<const R>> &&
    std::same_as<std::ranges::sentinel_t<R>, std::ranges::sentinel_t<const R>>;
// clang-format on

template<std::ranges::input_range V>
    requires std::ranges::view<V>
class enumerate_view : public std::ranges::view_interface<enumerate_view<V>> {

    V base_ = {};

    template<bool>
    struct sentinel;

    template<bool Const>
    struct iterator {
    private:
        using Base = std::conditional_t<Const, const V, V>;
        using count_type = decltype([] {
            if constexpr (std::ranges::sized_range<Base>)
                return std::ranges::range_size_t<Base>();
            else {
                return std::make_unsigned_t<std::ranges::range_difference_t<Base>>();
            }
        }());

        template<typename T>
        struct result {
            const count_type index;
            T value;

            constexpr bool operator==(const result& other) const = default;
        };

        std::ranges::iterator_t<Base> current_ = std::ranges::iterator_t<Base>();
        count_type pos_ = 0;

        template<bool>
        friend struct iterator;
        template<bool>
        friend struct sentinel;

    public:
        using iterator_category = decltype(detail::iter_cat<Base>());
        using reference = result<std::ranges::range_reference_t<Base>>;
        using value_type = result<std::ranges::range_reference_t<Base>>;
        using difference_type = std::ranges::range_difference_t<Base>;

        iterator() = default;

        constexpr explicit iterator(std::ranges::iterator_t<Base> current,
                                    std::ranges::range_difference_t<Base> pos) :
            current_(std::move(current)),
            pos_(pos) {}
        constexpr explicit iterator(iterator<!Const> i)
            requires Const && std::convertible_to<std::ranges::iterator_t<V>,
                                                  std::ranges::iterator_t<Base>>
            : current_(std::move(i.current_)), pos_(i.pos_) {}

        constexpr const std::ranges::iterator_t<V>& base() const&
            requires std::copyable<std::ranges::iterator_t<Base>>
        {
            return current_;
        }

        constexpr std::ranges::iterator_t<V> base() && { return std::move(current_); }

        constexpr auto operator*() const {
            return reference{static_cast<count_type>(pos_), *current_};
        }

        constexpr iterator& operator++() {
            ++pos_;
            ++current_;
            return *this;
        }

        constexpr auto operator++(int) {
            ++pos_;
            if constexpr (std::ranges::forward_range<V>) {
                auto tmp = *this;
                ++*this;
                return tmp;
            } else {
                ++current_;
            }
        }

        constexpr iterator& operator--()
            requires std::ranges::bidirectional_range<V>
        {
            --pos_;
            --current_;
            return *this;
        }

        constexpr auto operator--(int)
            requires std::ranges::bidirectional_range<V>
        {
            auto tmp = *this;
            --*this;
            return tmp;
        }

        constexpr iterator& operator+=(difference_type n)
            requires std::ranges::random_access_range<V>
        {
            current_ += n;
            pos_ += n;
            return *this;
        }

        constexpr iterator& operator-=(difference_type n)
            requires std::ranges::random_access_range<V>
        {
            current_ -= n;
            pos_ -= n;
            return *this;
        }

        friend constexpr iterator operator+(const iterator& i, difference_type n)
            requires std::ranges::random_access_range<V>
        {
            return iterator{i.current_ + n, static_cast<difference_type>(i.pos_ + n)};
        }

        friend constexpr iterator operator+(difference_type n, const iterator& i)
            requires std::ranges::random_access_range<V>
        {
            return iterator{i.current_ + n, static_cast<difference_type>(i.pos_ + n)};
        }

        friend constexpr auto operator-(iterator i, difference_type n)
            requires std::ranges::random_access_range<V>
        {
            return iterator{i.current_ - n, static_cast<difference_type>(i.pos_ - n)};
        }

        friend constexpr auto operator-(difference_type n, iterator i)
            requires std::ranges::random_access_range<V>
        {
            return iterator{i.current_ - n, static_cast<difference_type>(i.pos_ - n)};
        }

        constexpr decltype(auto) operator[](difference_type n) const
            requires std::ranges::random_access_range<Base>
        {
            return reference{static_cast<count_type>(pos_ + n), *(current_ + n)};
        }

        friend constexpr bool operator==(const iterator& x, const iterator& y)
            requires std::equality_comparable<std::ranges::iterator_t<Base>>
        {
            return x.current_ == y.current_;
        }

        template<bool ConstS>
        friend constexpr bool operator==(const iterator<Const>& i, const sentinel<ConstS>& s) {
            return i.current_ == s.base();
        }

        friend constexpr bool operator<(const iterator& x, const iterator& y)
            requires std::ranges::random_access_range<Base>
        {
            return x.current_ < y.current_;
        }

        friend constexpr bool operator>(const iterator& x, const iterator& y)
            requires std::ranges::random_access_range<Base>
        {
            return x.current_ > y.current_;
        }

        friend constexpr bool operator<=(const iterator& x, const iterator& y)
            requires std::ranges::random_access_range<Base>
        {
            return x.current_ <= y.current_;
        }
        friend constexpr bool operator>=(const iterator& x, const iterator& y)
            requires std::ranges::random_access_range<Base>
        {
            return x.current_ >= y.current_;
        }
        friend constexpr auto operator<=>(const iterator& x, const iterator& y)
            requires std::ranges::random_access_range<Base> &&
                     std::three_way_comparable<std::ranges::iterator_t<Base>>
        {
            return x.current_ <=> y.current_;
        }

        friend constexpr difference_type operator-(const iterator& x, const iterator& y)
            requires std::ranges::random_access_range<Base>
        {
            return x.current_ - y.current_;
        }
    };

    template<bool Const>
    struct sentinel {
    private:
        friend iterator<false>;
        friend iterator<true>;

        using Base = std::conditional_t<Const, const V, V>;

        std::ranges::sentinel_t<V> end_;

    public:
        sentinel() = default;

        constexpr explicit sentinel(std::ranges::sentinel_t<V> end) : end_(std::move(end)) {}

        constexpr auto base() const { return end_; }

        friend constexpr std::ranges::range_difference_t<Base> operator-(const iterator<Const>& x,
                                                                         const sentinel& y)
            requires std::sized_sentinel_for<std::ranges::sentinel_t<Base>,
                                             std::ranges::iterator_t<Base>>
        {
            return x.current_ - y.end_;
        }

        friend constexpr std::ranges::range_difference_t<Base> operator-(const sentinel& x,
                                                                         const iterator<Const>& y)
            requires std::sized_sentinel_for<std::ranges::sentinel_t<Base>,
                                             std::ranges::iterator_t<Base>>
        {
            return x.end_ - y.current_;
        }
    };

public:
    constexpr enumerate_view() = default;
    constexpr explicit enumerate_view(V base) : base_(std::move(base)) {}

    constexpr auto begin()
        requires(!simple_view<V>)
    {
        return iterator<false>(std::ranges::begin(base_), 0);
    }

    constexpr auto begin() const
        requires simple_view<V>
    {
        return iterator<true>(std::ranges::begin(base_), 0);
    }

    constexpr auto end() { return sentinel<false>{std::ranges::end(base_)}; }

    constexpr auto end()
        requires std::ranges::common_range<V>
    {
        return iterator<true>{std::ranges::end(base_),
                              static_cast<std::ranges::range_difference_t<V>>(size())};
    }

    constexpr auto end() const
        requires std::ranges::range<const V>
    {
        return sentinel<true>{std::ranges::end(base_)};
    }

    constexpr auto end() const
        requires std::ranges::common_range<const V>
    {
        return iterator<true>{std::ranges::end(base_),
                              static_cast<std::ranges::range_difference_t<V>>(size())};
    }

    constexpr auto size()
        requires std::ranges::sized_range<V>
    {
        return std::ranges::size(base_);
    }

    constexpr auto size() const
        requires std::ranges::sized_range<const V>
    {
        return std::ranges::size(base_);
    }

    constexpr V base() const&
        requires std::copyable<V>
    {
        return base_;
    }

    constexpr V base() && { return std::move(base_); }
};

template<typename R>
    requires std::ranges::input_range<R>
enumerate_view(R&& r) -> enumerate_view<std::ranges::views::all_t<R>>;

namespace detail {

struct enumerate_view_fn {
    template<typename R>
    constexpr auto operator()(R&& r) const {
        return enumerate_view{std::forward<R>(r)};
    }

    template<typename T>
    constexpr auto operator()(std::initializer_list<T> list) {
        return enumerate_view{list};
    }

    template<std::ranges::input_range R>
    constexpr friend auto operator|(R&& rng, const enumerate_view_fn&) {
        return enumerate_view{std::forward<R>(rng)};
    }
};
} // namespace detail

inline detail::enumerate_view_fn enumerate;

} // namespace satext::ranges
