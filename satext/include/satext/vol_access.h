#pragma once
#include <type_traits>

namespace satext {

template<typename T>
constexpr inline T volatile_load(const T* p) noexcept {
    return *static_cast<const volatile T*>(p);
}

template<typename T>
constexpr inline void volatile_store(T* p, T value) noexcept {
    *static_cast<volatile T*>(p) = value;
}
} // namespace satext
