#pragma once
#include <type_traits>

namespace satext {

template<typename T>
constexpr auto to_underlying_type(T v) {
    return static_cast<std::underlying_type_t<T>>(v);
}

} // namespace satext
