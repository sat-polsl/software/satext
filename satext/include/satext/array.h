#pragma once
#include <array>
#include <type_traits>

namespace satext {

namespace detail {
template<typename T, std::size_t... Is>
constexpr std::array<T, sizeof...(Is)> create_array(T value, std::index_sequence<Is...>) {
    return {(static_cast<void>(Is), value)...};
}

template<typename T, typename... Args, std::size_t... Is>
constexpr std::array<T, sizeof...(Is)> create_array(std::index_sequence<Is...>, Args&&... args) {
    static_assert(std::is_constructible_v<T, Args...>,
                  "Number of arguments does not match any constructor of T");
    return {(static_cast<void>(Is), T(std::forward<Args>(args)...))...};
}
} // namespace detail

template<std::size_t N, typename T>
constexpr std::array<T, N> create_array(const T& value) {
    return detail::create_array(value, std::make_index_sequence<N>());
}

template<typename T, std::size_t N, typename... Args>
constexpr std::array<T, N> create_array(Args&&... args) {
    return detail::create_array<T>(std::make_index_sequence<N>(), std::forward<Args>(args)...);
}
} // namespace satext