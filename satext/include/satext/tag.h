#include <concepts>
#include <functional>
#include <type_traits>
#include <utility>

namespace satext {

template<typename T, typename Tag>
class tag {
public:
    using value = std::remove_reference_t<std::remove_pointer_t<T>>;
    using reference = std::add_lvalue_reference_t<value>;
    using rvalue_reference = std::add_rvalue_reference_t<value>;
    using const_reference = std::add_lvalue_reference_t<std::add_const_t<value>>;
    using pointer = std::add_pointer_t<value>;
    using const_pointer = std::add_pointer_t<std::add_const_t<value>>;
    tag() = default;

    template<typename... Args>
    tag(Args&&... args) : t_{std::forward<Args>(args)...} {}

    tag(const reference t) : t_{t} {}

    tag(rvalue_reference t) : t_{std::move(t)} {}

    reference get() { return t_; }

    const_reference get() const { return t_; }

    reference operator*() { return t_; }

    const_reference operator*() const { return t_; }

    pointer operator->() { return std::addressof(t_); }

    const_pointer operator->() const { return std::addressof(t_); }

private:
    T t_{};
};

template<typename T, typename Tag>
class tag_ptr {
public:
    using value = std::remove_reference_t<std::remove_pointer_t<T>>;
    using reference = std::add_lvalue_reference_t<value>;
    using rvalue_reference = std::add_rvalue_reference_t<value>;
    using const_reference = std::add_lvalue_reference_t<std::add_const_t<value>>;
    using pointer = std::add_pointer_t<value>;
    using const_pointer = std::add_pointer_t<std::add_const_t<value>>;

    tag_ptr() = default;

    explicit tag_ptr(const reference t) : ptr_{std::addressof(t)} {}

    explicit tag_ptr(pointer ptr) : ptr_{ptr} {}

    reference get() { return *ptr_; }

    const_reference get() const { return *ptr_; }

    reference operator*() { return *ptr_; }

    const_reference operator*() const { return *ptr_; }

    pointer operator->() { return ptr_; }

    const_pointer operator->() const { return ptr_; }

private:
    pointer ptr_{nullptr};
};

} // namespace satext
