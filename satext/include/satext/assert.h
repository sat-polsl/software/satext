#pragma once
#include <exception>

namespace satext {

template<typename Expression>
constexpr void expects(const Expression& expression) {
    static_cast<bool>(expression) ? static_cast<void>(0) : std::terminate();
}

template<typename Expression>
constexpr void ensures(const Expression& expression) {
    static_cast<bool>(expression) ? static_cast<void>(0) : std::terminate();
}

} // namespace satext
