#pragma once

#include <tuple>
#include <utility>

#include "satext/struct/format.h"

namespace satext::struct_detail {

template<typename Fmt>
constexpr std::size_t calcsize(Fmt formatString);

template<typename Fmt>
constexpr std::size_t calcsize(Fmt) {
    constexpr auto num_items = count_items(Fmt{});
    constexpr auto last_item = get_type_of_item<num_items - 1>(Fmt{});

    return get_binary_offset<num_items - 1>(Fmt{}) + last_item.size;
}

} // namespace satext::struct_detail
