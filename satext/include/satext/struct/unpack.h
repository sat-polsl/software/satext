#pragma once

#include <array>
#include <variant>

#include "satext/expected.h"
#include "satext/struct/calcsize.h"
#include "satext/struct/data_view.h"

namespace satext {

template<typename Fmt, typename Input>
constexpr auto unpack(Fmt, Input&& packedInput);

namespace struct_detail {

namespace detail {

template<typename Fmt, std::size_t... Items, typename Input>
constexpr auto unpack(std::index_sequence<Items...>, Input&& packed_input);

} // namespace detail

template<std::size_t Item, typename UnpackedType>
constexpr auto unpack_element(const std::byte* begin, std::size_t size, bool big_endian) {
    data_view<const std::byte> view(begin, big_endian);
    view.size = size;

    return data::get<UnpackedType>(view);
}

template<typename Fmt, std::size_t... Items, typename Input>
constexpr auto detail::unpack(std::index_sequence<Items...>, Input&& packed_input) {
    constexpr auto fmt_mode = struct_detail::get_format_mode(Fmt{});

    constexpr format_type formats[] = {struct_detail::get_type_of_item<Items>(Fmt{})...};
    constexpr auto total_size = get_total_size(formats);

    using Types = std::tuple<
        typename struct_detail::RepresentedType<decltype(fmt_mode), formats[Items].format_char>...>;

    constexpr std::size_t offsets[] = {get_binary_offset<Items>(Fmt{})...};

    auto unpacked = std::make_tuple(unpack_element<Items, std::tuple_element_t<Items, Types>>(
        std::data(packed_input) + offsets[Items],
        formats[Items].size,
        fmt_mode.is_big_endian())...);

    using return_type = satext::expected<decltype(unpacked), std::monostate>;

    if (std::size(packed_input) < total_size) {
        return return_type{satext::unexpected{std::monostate{}}};
    }
    return return_type{unpacked};
}

} // namespace struct_detail

template<typename Fmt, typename Input>
constexpr auto unpack(Fmt, Input&& packedInput) {
    return struct_detail::detail::unpack<Fmt>(
        std::make_index_sequence<struct_detail::count_items(Fmt{})>(),
        std::forward<Input>(packedInput));
}

} // namespace satext
