#pragma once

#include <iterator>
#include <satext/basic_fixed_string.h>
#include <utility>

namespace satext::struct_detail {
namespace detail {

struct format_string {};

constexpr bool is_digit(char ch) { return ch >= '0' && ch <= '9'; }

template<std::size_t Size>
constexpr std::pair<std::size_t, std::size_t> consume_number(const char (&str)[Size],
                                                             std::size_t offset) {
    std::size_t num = 0;
    std::size_t i = offset;
    for (; is_digit(str[i]) && i < Size; i++) {
        num = static_cast<std::size_t>(num * 10 + (str[i] - '0'));
    }

    return {num, i};
}

constexpr std::pair<std::size_t, std::size_t>
consume_number(const char* str, std::size_t size, std::size_t offset) {
    std::size_t num = 0;
    std::size_t i = offset;
    for (; is_digit(str[i]) && i < size; i++) {
        num = static_cast<std::size_t>(num * 10 + (str[i] - '0'));
    }

    return {num, i};
}

} // namespace detail
} // namespace satext::struct_detail

namespace satext::struct_literals {
template<satext::basic_fixed_string str>
auto operator"" _fmt() {
    struct S : satext::struct_detail::detail::format_string {
        static constexpr auto value() { return str.buffer; }

        static constexpr std::size_t size() { return str.size; }

        static constexpr auto at(std::size_t i) { return value()[i]; };
    };
    return S{};
}
} // namespace satext::struct_literals
