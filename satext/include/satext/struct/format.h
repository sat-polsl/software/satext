#pragma once

#include "satext/struct/string.h"

#include <cstdint>
#include <string_view>

namespace satext::struct_detail {

constexpr bool is_format_mode(char format_char) {
    return format_char == '<' || format_char == '>' || format_char == '!' || format_char == '=' ||
           format_char == '@';
}

constexpr bool is_format_char(char format_char) {
    return is_format_mode(format_char) || format_char == 'x' || format_char == 'b' ||
           format_char == 'B' || format_char == 'c' || format_char == 's' || format_char == 'h' ||
           format_char == 'H' || format_char == 'i' || format_char == 'I' || format_char == 'l' ||
           format_char == 'L' || format_char == 'q' || format_char == 'Q' || format_char == 'f' ||
           format_char == 'd' || format_char == '?' || detail::is_digit(format_char);
}

template<char FormatChar>
struct format_mode {
    static_assert(is_format_mode(FormatChar), "Invalid Format Mode passed");

    static constexpr bool is_big_endian() { return false; }

    static constexpr bool should_pad() { return false; }

    static constexpr bool is_native() { return false; }
};

#define SET_FORMAT_MODE(mode, padding, bigEndian, native) \
    template<> \
    struct format_mode<mode> { \
        static constexpr bool is_big_endian() { return bigEndian; }; \
        static constexpr bool should_pad() { return padding; }; \
        static constexpr bool is_native() { return native; } \
    }

SET_FORMAT_MODE('@', true, false, true);

SET_FORMAT_MODE('>', false, true, false);

SET_FORMAT_MODE('!', false, true, false);

template<char FormatChar>
struct big_endian_format {
    static_assert(is_format_char(FormatChar), "Invalid Format Char passed");

    static constexpr std::size_t size() { return 0; }
};

#define SET_FORMAT_CHAR(ch, s, rep_type, native_rep_type) \
    template<> \
    struct big_endian_format<ch> { \
        static constexpr std::size_t size() { return s; } \
        static constexpr std::size_t native_size() { return sizeof(native_rep_type); } \
        using RepresentedType = rep_type; \
        using NativeRepresentedType = native_rep_type; \
    }

template<typename Fmt, char FormatChar>
using RepresentedType =
    std::conditional_t<Fmt::is_native(),
                       typename big_endian_format<FormatChar>::NativeRepresentedType,
                       typename big_endian_format<FormatChar>::RepresentedType>;

SET_FORMAT_CHAR('?', 1, bool, bool);

SET_FORMAT_CHAR('x', 1, char, char);

SET_FORMAT_CHAR('b', 1, int8_t, signed char);

SET_FORMAT_CHAR('B', 1, uint8_t, unsigned char);

SET_FORMAT_CHAR('c', 1, char, char);

template<>
struct big_endian_format<'s'> {
    static constexpr std::size_t size() { return sizeof(char); }

    static constexpr std::size_t native_size() { return sizeof(char); }

    using RepresentedType = std::string_view;
    using NativeRepresentedType = std::string_view;
};

SET_FORMAT_CHAR('h', 2, int16_t, short);

SET_FORMAT_CHAR('H', 2, uint16_t, unsigned short);

SET_FORMAT_CHAR('i', 4, int32_t, int);

SET_FORMAT_CHAR('I', 4, uint32_t, unsigned int);

SET_FORMAT_CHAR('l', 4, int32_t, long);

SET_FORMAT_CHAR('L', 4, uint32_t, unsigned long);

SET_FORMAT_CHAR('q', 8, int64_t, long long);

SET_FORMAT_CHAR('Q', 8, uint64_t, unsigned long long);

SET_FORMAT_CHAR('f', 4, float, float);

SET_FORMAT_CHAR('d', 8, double, double);

template<typename Fmt>
constexpr auto get_format_mode(Fmt) {
    if constexpr (is_format_mode(Fmt::at(0))) {
        constexpr auto firstChar = Fmt::at(0);
        return format_mode<firstChar>{};
    } else {
        return format_mode<'@'>{};
    }
}

struct format_type {
    char format_char;
    std::size_t repeat;
    std::size_t format_size;
    std::size_t size;
    std::size_t offset;

    constexpr bool is_string() { return format_char == 's'; }
};

constexpr bool does_format_align(format_type format) { return format.format_size > 1; }

template<typename Fmt, char FormatChar, std::size_t Repeat = 1>
constexpr std::size_t get_size() {
    if constexpr (get_format_mode(Fmt{}).is_native()) {
        return big_endian_format<FormatChar>::native_size() * Repeat;
    } else {
        return big_endian_format<FormatChar>::size() * Repeat;
    }
}

template<typename Fmt>
constexpr std::size_t get_size(char format_char, std::size_t repeat = 1) {
    switch (format_char) {
    case '?':
        return get_size<Fmt, '?', 1>() * repeat;
    case 'x':
        return get_size<Fmt, 'x', 1>() * repeat;
    case 'b':
        return get_size<Fmt, 'b', 1>() * repeat;
    case 'B':
        return get_size<Fmt, 'B', 1>() * repeat;
    case 'c':
        return get_size<Fmt, 'c', 1>() * repeat;
    case 's':
        return get_size<Fmt, 's', 1>() * repeat;
    case 'h':
        return get_size<Fmt, 'h', 1>() * repeat;
    case 'H':
        return get_size<Fmt, 'H', 1>() * repeat;
    case 'i':
        return get_size<Fmt, 'i', 1>() * repeat;
    case 'I':
        return get_size<Fmt, 'I', 1>() * repeat;
    case 'l':
        return get_size<Fmt, 'l', 1>() * repeat;
    case 'L':
        return get_size<Fmt, 'L', 1>() * repeat;
    case 'q':
        return get_size<Fmt, 'q', 1>() * repeat;
    case 'Q':
        return get_size<Fmt, 'Q', 1>() * repeat;
    case 'f':
        return get_size<Fmt, 'f', 1>() * repeat;
    case 'd':
        return get_size<Fmt, 'd', 1>() * repeat;
    default:
        return 0;
    }
}

template<typename Fmt>
constexpr std::size_t count_items(Fmt) {
    std::size_t item_count = 0;

    std::size_t multiplier = 1;
    for (std::size_t i = 0; i < Fmt::size(); i++) {
        auto current_char = Fmt::at(i);
        if (i == 0 && is_format_mode(current_char)) {
            continue;
        }

        if (detail::is_digit(current_char)) {
            auto numberAndOffset = detail::consume_number(Fmt::value(), Fmt::size(), i);

            multiplier = numberAndOffset.first;
            i = numberAndOffset.second;
            i--; // to combat the i++ in the loop
            continue;
        }

        if (current_char == 's') {
            item_count++;
        } else if (current_char != 'x') {
            item_count += multiplier;
        }
        multiplier = 1;
    }

    return item_count;
}

template<std::size_t Item, std::size_t ArrSize>
constexpr format_type get_unwrapped_item(format_type (&wrapped_formats)[ArrSize]) {
    std::size_t current_item = 0;
    for (std::size_t i = 0; i < ArrSize; i++) {
        for (std::size_t repeat = 0; repeat < wrapped_formats[i].repeat; repeat++) {
            auto current_type = wrapped_formats[i];
            if (current_item == Item) {
                if (!current_type.is_string()) {
                    current_type.repeat = 1;
                }
                current_type.offset += repeat * current_type.format_size;
                return current_type;
            }

            current_item++;
            if (current_type.is_string()) {
                break;
            }
        }
    }

    // cannot get here, Item < ArrSize
    return {0, 0};
}

template<std::size_t Item, typename Fmt, std::size_t... Is>
constexpr format_type get_type_of_item(std::index_sequence<Is...>) {
    constexpr char format_string[] = {Fmt::at(Is)...};
    format_type wrapped_types[count_items(Fmt{})]{};

    std::size_t current_type = 0;
    std::size_t offset = 0;

    for (std::size_t i = 0; i < sizeof...(Is); i++) {
        if (is_format_mode(format_string[i])) {
            continue;
        }

        auto repeat_count = detail::consume_number(format_string, i);
        auto repeat = repeat_count.first;
        i = repeat_count.second;

        if (repeat_count.first == 0) {
            repeat = 1;
        }

        if (format_string[i] == 'x') {
            offset += repeat;
            continue;
        }

        wrapped_types[current_type].format_char = format_string[i];
        wrapped_types[current_type].repeat = repeat;
        wrapped_types[current_type].format_size = get_size<Fmt>(format_string[i]);
        wrapped_types[current_type].size = get_size<Fmt>(format_string[i], repeat);
        wrapped_types[current_type].offset = offset;
        offset += wrapped_types[current_type].size;

        current_type++;
    }

    return get_unwrapped_item<Item>(wrapped_types);
}

template<std::size_t Item, typename Fmt>
constexpr format_type get_type_of_item(Fmt) {
    static_assert(Item < count_items(Fmt{}), "Item requested must be inside the format");
    constexpr format_type format =
        get_type_of_item<Item, Fmt>(std::make_index_sequence<Fmt::size()>());

    return format;
}

template<typename Fmt, std::size_t... Items>
constexpr std::size_t get_binary_offset(Fmt, std::index_sequence<Items...>) {
    constexpr format_type item_types[] = {get_type_of_item<Items>(Fmt{})...};

//    constexpr auto format_mode = struct_detail::get_format_mode(Fmt{});
//
//    std::size_t size = 0;
//    for (std::size_t i = 0; i < sizeof...(Items) - 1; i++) {
//        size += item_types[i].size;
//
//        if (format_mode.should_pad()) {
//            if (does_format_align(item_types[i + 1])) {
//                auto current_alignment = (size % item_types[i + 1].format_size);
//                if (current_alignment != 0) {
//                    size += item_types[i + 1].format_size - current_alignment;
//                }
//            }
//        }
//    }

    return item_types[sizeof...(Items) - 1].offset;
}

template<std::size_t Item, typename Fmt>
constexpr std::size_t get_binary_offset(Fmt) {
    return get_binary_offset(Fmt{}, std::make_index_sequence<Item + 1>());
}

template<std::size_t Size>
constexpr std::size_t get_total_size(const format_type (&item_types)[Size]) {
    return item_types[Size - 1].offset + item_types[Size - 1].size;
}

} // namespace satext::struct_detail
