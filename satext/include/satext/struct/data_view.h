#pragma once

#include <algorithm>
#include <cstdint>
#include <cstring>
#include <string_view>

namespace satext::struct_detail {

template<typename T>
struct data_view {
    constexpr data_view(T* b, bool bigEndian) : is_big_endian(bigEndian), bytes(b) {}

    std::size_t size = 0;
    bool is_big_endian;
    T* bytes;
};

namespace data {

namespace impl {

constexpr void store(data_view<std::byte>& d, uint8_t v) { d.bytes[0] = std::byte(v & 0xFF); }

constexpr void store(data_view<std::byte>& d, bool v) { d.bytes[0] = static_cast<std::byte>(v); }

constexpr void store(data_view<std::byte>& d, uint16_t v) {
    if (!d.is_big_endian) {
        d.bytes[0] = std::byte(v & 0xFF);
        d.bytes[1] = std::byte((v >> 8) & 0xFF);
    } else {
        d.bytes[1] = std::byte(v & 0xFF);
        d.bytes[0] = std::byte((v >> 8) & 0xFF);
    }
}

constexpr void store(data_view<std::byte>& d, uint32_t v) {
    if (!d.is_big_endian) {
        d.bytes[0] = std::byte(v & 0xFF);
        d.bytes[1] = std::byte((v >> 8) & 0xFF);
        d.bytes[2] = std::byte((v >> 16) & 0xFF);
        d.bytes[3] = std::byte((v >> 24) & 0xFF);
    } else {
        d.bytes[3] = std::byte(v & 0xFF);
        d.bytes[2] = std::byte((v >> 8) & 0xFF);
        d.bytes[1] = std::byte((v >> 16) & 0xFF);
        d.bytes[0] = std::byte((v >> 24) & 0xFF);
    }
}

constexpr void store(data_view<std::byte>& d, uint64_t v) {
    if (!d.is_big_endian) {
        d.bytes[0] = std::byte(v & 0xFF);
        d.bytes[1] = std::byte((v >> 8) & 0xFF);
        d.bytes[2] = std::byte((v >> 16) & 0xFF);
        d.bytes[3] = std::byte((v >> 24) & 0xFF);
        d.bytes[4] = std::byte((v >> 32) & 0xFF);
        d.bytes[5] = std::byte((v >> 40) & 0xFF);
        d.bytes[6] = std::byte((v >> 48) & 0xFF);
        d.bytes[7] = std::byte((v >> 56) & 0xFF);
    } else {
        d.bytes[7] = std::byte(v & 0xFF);
        d.bytes[6] = std::byte((v >> 8) & 0xFF);
        d.bytes[5] = std::byte((v >> 16) & 0xFF);
        d.bytes[4] = std::byte((v >> 24) & 0xFF);
        d.bytes[3] = std::byte((v >> 32) & 0xFF);
        d.bytes[2] = std::byte((v >> 40) & 0xFF);
        d.bytes[1] = std::byte((v >> 48) & 0xFF);
        d.bytes[0] = std::byte((v >> 56) & 0xFF);
    }
}

constexpr void store(data_view<std::byte>& d, int8_t v) {
    uint8_t b = 0;

    if (v > 0) {
        b = v;
    } else {
        b = 0xFF + v + 1;
    }

    store(d, b);
}

constexpr void store(data_view<std::byte>& d, int16_t v) {
    uint16_t b = 0;

    if (v > 0) {
        b = v;
    } else {
        b = 0xFFFF + v + 1;
    }

    store(d, b);
}

constexpr void store(data_view<std::byte>& d, int32_t v) {
    uint32_t b = 0;

    if (v > 0) {
        b = v;
    } else {
        b = static_cast<uint32_t>(0xFFFFFFFFULL + static_cast<uint32_t>(v) + 1ull);
    }

    store(d, b);
}

constexpr void store(data_view<std::byte>& d, int64_t v) {
    uint64_t b = 0;

    if (v > 0) {
        b = v;
    } else {
        b = 0xFFFFFFFFFFFFFFFFULL + static_cast<uint64_t>(v) + 1ull;
    }

    store(d, b);
}

inline void store(data_view<std::byte>& d, double v) {
    *(double*)d.bytes = v;
    if (d.is_big_endian) {
        std::reverse(d.bytes, d.bytes + sizeof(double));
    }
}

inline void store(data_view<std::byte>& d, float f) {
    *(float*)d.bytes = f;
    if (d.is_big_endian) {
        std::reverse(d.bytes, d.bytes + sizeof(float));
    }
}

constexpr void store(data_view<std::byte>& d, std::string_view str) {
    for (std::size_t i = 0; i < str.size(); i++) {
        d.bytes[i] = static_cast<std::byte>(str[i]);
    }
}

template<typename T>
constexpr T get(const data_view<const std::byte>& d);

template<>
constexpr uint8_t get(const data_view<const std::byte>& d) {
    return static_cast<uint8_t>(d.bytes[0] & std::byte{0xff});
}

template<>
constexpr bool get(const data_view<const std::byte>& d) {
    return get<uint8_t>(d) != '\0';
}

template<>
constexpr uint16_t get(const data_view<const std::byte>& d) {
    uint16_t v = 0;
    if (!d.is_big_endian) {
        v += static_cast<uint16_t>(static_cast<uint8_t>(d.bytes[0] & std::byte{0xFF}));
        v += static_cast<uint16_t>(static_cast<uint8_t>(d.bytes[1] & std::byte{0xFF}) << 8);
    } else {
        v += static_cast<uint16_t>(static_cast<uint8_t>(d.bytes[1] & std::byte{0xFF}));
        v += static_cast<uint16_t>(static_cast<uint8_t>(d.bytes[0] & std::byte{0xFF}) << 8);
    }

    return v;
}

template<>
constexpr uint32_t get(const data_view<const std::byte>& d) {
    uint32_t v = 0;
    if (!d.is_big_endian) {
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[0] & std::byte{0xFF}));
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[1] & std::byte{0xFF}) << 8);
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[2] & std::byte{0xFF}) << 16);
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[3] & std::byte{0xFF}) << 24);
    } else {
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[3] & std::byte{0xFF}));
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[2] & std::byte{0xFF}) << 8);
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[1] & std::byte{0xFF}) << 16);
        v += static_cast<uint32_t>(static_cast<uint8_t>(d.bytes[0] & std::byte{0xFF}) << 24);
    }

    return v;
}

template<>
constexpr uint64_t get(const data_view<const std::byte>& d) {
    uint64_t v = 0;
    if (!d.is_big_endian) {
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[0] & std::byte{0xFF}));
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[1] & std::byte{0xFF}) << 8ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[2] & std::byte{0xFF}) << 16ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[3] & std::byte{0xFF}) << 24ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[4] & std::byte{0xFF}) << 32ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[5] & std::byte{0xFF}) << 40ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[6] & std::byte{0xFF}) << 48ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[7] & std::byte{0xFF}) << 56ULL);
    } else {
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[7] & std::byte{0xFF}));
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[6] & std::byte{0xFF}) << 8ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[5] & std::byte{0xFF}) << 16ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[4] & std::byte{0xFF}) << 24ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[3] & std::byte{0xFF}) << 32ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[2] & std::byte{0xFF}) << 40ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[1] & std::byte{0xFF}) << 48ULL);
        v += static_cast<uint64_t>(static_cast<uint64_t>(d.bytes[0] & std::byte{0xFF}) << 56ULL);
    }

    return v;
}

template<>
constexpr int8_t get(const data_view<const std::byte>& d) {
    uint8_t b = get<unsigned char>(d);
    return static_cast<int8_t>(b - 0xFFULL - 1);
}

template<>
constexpr int16_t get(const data_view<const std::byte>& d) {
    uint16_t b = get<uint16_t>(d);
    return static_cast<int16_t>(b - 0xFFFFULL - 1);
}

template<>
constexpr int32_t get(const data_view<const std::byte>& d) {
    uint32_t b = get<uint32_t>(d);
    return static_cast<int32_t>(b - 0xFFFFFFFFULL - 1);
}

template<>
constexpr int64_t get(const data_view<const std::byte>& d) {
    uint64_t b = get<uint64_t>(d);
    return static_cast<int64_t>(b - 0xFFFFFFFFFFFFFFFFULL - 1);
}

template<>
inline double get(const data_view<const std::byte>& d) {
    double v = *(double*)d.bytes;
    if (d.is_big_endian) {
        std::reverse((std::byte*)&v, (std::byte*)&v + sizeof(double));
    }

    return v;
}

template<>
inline float get(const data_view<const std::byte>& d) {
    float v = *(float*)d.bytes;
    if (d.is_big_endian) {
        std::reverse((std::byte*)&v, (std::byte*)&v + sizeof(float));
    }

    return v;
}

template<>
inline std::string_view get(const data_view<const std::byte>& d) {
    return {(char*)d.bytes, d.size};
}

template<std::size_t Bytes>
struct unsinged_integer;

template<>
struct unsinged_integer<1> {
    using type = uint8_t;
};

template<>
struct unsinged_integer<2> {
    using type = uint16_t;
};

template<>
struct unsinged_integer<4> {
    using type = uint32_t;
};

template<>
struct unsinged_integer<8> {
    using type = uint64_t;
};

template<typename T>
using integer_type =
    std::conditional_t<std::is_signed_v<T>,
                       typename std::make_signed_t<typename unsinged_integer<sizeof(T)>::type>,
                       typename unsinged_integer<sizeof(T)>::type>;

} // namespace impl

template<typename T>
constexpr void store(data_view<std::byte>& d, T v) {
    if constexpr (std::is_integral_v<T>) {
        return impl::store(d, static_cast<impl::integer_type<T>>(v));
    } else {
        return impl::store(d, v);
    }
}

template<typename T>
constexpr auto get(const data_view<const std::byte>& d) {
    if constexpr (std::is_integral_v<T> && !std::is_same_v<T, bool>) {
        return impl::get<impl::integer_type<T>>(d);
    } else {
        return impl::get<T>(d);
    }
}

} // namespace data
} // namespace satext::struct_detail
