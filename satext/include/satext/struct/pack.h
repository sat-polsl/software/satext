#pragma once

#include <array>
#include <span>

#include "satext/struct/calcsize.h"
#include "satext/struct/data_view.h"

namespace satext {
namespace struct_detail {

namespace detail {
template<typename RepType>
constexpr int pack_element(std::byte* data, bool big_endian, format_type format, RepType elem) {
    if constexpr (std::is_same_v<RepType, std::string_view>) {
        elem = std::string_view(elem.data(), std::min(elem.size(), format.size));
    } else {
        static_cast<void>(format);
    }

    data_view<std::byte> view(data, big_endian);

    data::store(view, elem);
    return 0;
}

template<typename RepType, typename T>
constexpr RepType convert(const T& val) {
    if constexpr (std::is_array_v<T> && std::is_same_v<std::remove_extent_t<T>, char> &&
                  std::is_same_v<RepType, std::string_view>) {
        return RepType(std::data(val), std::size(val));
    } else {
        return static_cast<RepType>(val);
    }
}

template<typename Fmt, std::size_t... Items, typename... Args>
constexpr auto pack(std::index_sequence<Items...>, Args&&... args) {
    static_assert(sizeof...(args) == sizeof...(Items),
                  "pack expected items for packing != sizeof...(args) passed");
    constexpr auto fmt_mode = struct_detail::get_format_mode(Fmt{});

    using ArrayType = std::array<std::byte, struct_detail::calcsize(Fmt{})>;
    ArrayType output{};

    constexpr format_type formats[] = {struct_detail::get_type_of_item<Items>(Fmt{})...};
    using Types = std::tuple<
        typename struct_detail::RepresentedType<decltype(fmt_mode), formats[Items].format_char>...>;

    Types t =
        std::make_tuple(convert<std::tuple_element_t<Items, Types>>(std::forward<Args>(args))...);

    constexpr std::size_t offsets[] = {get_binary_offset<Items>(Fmt{})...};
    int _[] = {0,
               pack_element(output.data() + offsets[Items],
                            fmt_mode.is_big_endian(),
                            formats[Items],
                            std::get<Items>(t))...};
    static_cast<void>(_);

    return output;
}

template<typename Fmt, std::size_t... Items, typename... Args>
std::size_t pack_to(std::index_sequence<Items...>, std::span<std::byte> buffer, Args&&... args) {
    static_assert(sizeof...(args) == sizeof...(Items),
                  "pack expected items for packing != sizeof...(args) passed");
    constexpr auto fmt_mode = struct_detail::get_format_mode(Fmt{});
    constexpr format_type formats[] = {struct_detail::get_type_of_item<Items>(Fmt{})...};
    constexpr auto total_size = get_total_size(formats);
    using Types = std::tuple<
        typename struct_detail::RepresentedType<decltype(fmt_mode), formats[Items].format_char>...>;
    Types t =
        std::make_tuple(convert<std::tuple_element_t<Items, Types>>(std::forward<Args>(args))...);
    constexpr std::size_t offsets[] = {get_binary_offset<Items>(Fmt{})...};
    if (buffer.size() < total_size) {
        return 0;
    }
    int _[] = {0,
               pack_element(buffer.data() + offsets[Items],
                            fmt_mode.is_big_endian(),
                            formats[Items],
                            std::get<Items>(t))...};
    static_cast<void>(_);
    return total_size;
}

} // namespace detail
} // namespace struct_detail

template<typename Fmt, typename... Args>
constexpr auto pack(Fmt formatString, Args&&... args);

template<typename Fmt, typename... Args>
constexpr auto pack(Fmt, Args&&... args) {
    constexpr std::size_t item_count = struct_detail::count_items(Fmt{});
    return struct_detail::detail::pack<Fmt>(std::make_index_sequence<item_count>(),
                                            std::forward<Args>(args)...);
}

template<typename Fmt, typename... Args>
std::size_t pack_to(Fmt, std::span<std::byte> buffer, Args&&... args) {
    constexpr std::size_t item_count = struct_detail::count_items(Fmt{});
    return struct_detail::detail::pack_to<Fmt>(
        std::make_index_sequence<item_count>(), buffer, std::forward<Args>(args)...);
}

} // namespace satext
