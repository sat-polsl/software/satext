#pragma once
#include <algorithm>
#include <array>
#include <concepts>
#include <cstdint>
#include <ranges>
#include <tuple>

namespace satext {
template<std::equality_comparable Key, typename Value, std::size_t Size>
class constexpr_map {
public:
    using key_type = Key;
    using mapped_type = Value;
    using value_type = std::pair<Key, Value>;
    using reference = value_type&;
    using const_reference = const value_type&;

    class iterator {
    public:
        using value_type = constexpr_map::value_type;
        using difference_type = std::ptrdiff_t;
        using pointer = value_type*;
        using reference = value_type&;
        using iterator_category = std::forward_iterator_tag;

        iterator() = default;
        constexpr iterator(const value_type* pos) noexcept : pos_{pos} {}

        constexpr bool operator==(const iterator& rhs) const noexcept { return pos_ == rhs.pos_; }

        constexpr bool operator!=(const iterator& rhs) const noexcept { return pos_ != rhs.pos_; }

        constexpr const iterator operator++(int) noexcept {
            auto result = *this;
            ++pos_;
            return result;
        }

        constexpr iterator& operator++() noexcept {
            ++pos_;
            return *this;
        }

        constexpr iterator& operator+=(std::ptrdiff_t n) noexcept {
            pos_ += n;
            return *this;
        }

        constexpr iterator& operator+(std::ptrdiff_t n) noexcept { return pos_ + n; }

        constexpr const iterator operator--(int) noexcept {
            auto result = *this;
            --pos_;
            return result;
        }

        constexpr iterator& operator--() noexcept {
            --pos_;
            return *this;
        }

        constexpr iterator& operator-=(std::ptrdiff_t n) noexcept {
            pos_ -= n;
            return *this;
        }

        constexpr iterator& operator-(std::ptrdiff_t n) noexcept { return pos_ - n; }

        constexpr const auto& operator*() const noexcept { return *pos_; }

        constexpr const auto* operator->() const noexcept { return pos_; }

    private:
        const value_type* pos_{nullptr};
    };

    constexpr constexpr_map(std::array<value_type, Size> map) : map_{map} {}

    constexpr constexpr_map(std::initializer_list<value_type> list) {
        std::copy_n(list.begin(), std::min(list.size(), Size), map_.begin());
    }

    constexpr const mapped_type& operator[](const key_type& key) const noexcept { return at(key); }

    [[nodiscard]] constexpr const mapped_type& at(const key_type& key) const noexcept {
        return find(key)->second;
    }

    constexpr std::size_t size() const noexcept { return Size; }

    [[nodiscard]] constexpr iterator find(const key_type& key) const noexcept {
        return std::ranges::find(map_, key, &value_type::first);
    }

    constexpr iterator begin() const noexcept { return map_.cbegin(); }

    constexpr iterator cbegin() const noexcept { return begin(); }

    constexpr iterator end() const noexcept { return map_.cend(); }

    constexpr iterator cend() const noexcept { return end(); }

    constexpr bool contains(const key_type& key) const noexcept { return find(key) != end(); }

private:
    std::array<value_type, Size> map_{};
};
} // namespace satext
