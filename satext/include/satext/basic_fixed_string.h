#pragma once
#include <cstdint>

namespace satext {

template<std::size_t N>
struct basic_fixed_string {
    constexpr basic_fixed_string(char const* str) {
        for (std::size_t i = 0; i != N; i++) {
            buffer[i] = str[i];
        }
    }

    constexpr operator char const*() const { return buffer; }
    char buffer[N + 1]{};
    std::size_t size = N;
};

template<std::size_t N>
basic_fixed_string(char const (&)[N]) -> basic_fixed_string<N - 1>;

} // namespace satext
