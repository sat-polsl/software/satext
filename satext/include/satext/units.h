#pragma once
#include <cstdint>

consteval std::int32_t operator"" _Hz(unsigned long long int value) { return value; }

consteval std::int32_t operator"" _KHz(unsigned long long int value) { return value * 1000_Hz; }

consteval std::int32_t operator"" _MHz(unsigned long long int value) { return value * 1000_KHz; }

consteval std::int32_t operator"" _Byte(unsigned long long int value) { return value; }

consteval std::int32_t operator"" _KiB(unsigned long long int value) { return value * 1024_Byte; }

consteval std::int32_t operator"" _MiB(unsigned long long int value) { return value * 1024_KiB; }
