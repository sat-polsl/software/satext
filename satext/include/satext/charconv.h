#pragma once
#include <charconv>
#include <span>
#include <string_view>
#include "satext/type_traits.h"

namespace satext {

template<typename T>
constexpr std::from_chars_result from_chars(std::string_view str, T& value) {
    std::int32_t base = 10;
    if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X')) {
        base = 16;
        str = str.substr(2);
    }
    return std::from_chars(str.data(), str.data() + str.size(), value, base);
}

template<typename T>
constexpr std::from_chars_result from_chars(std::string_view str, T& value, std::int32_t base) {
    return std::from_chars(str.data(), str.data() + str.size(), value, base);
}

constexpr std::from_chars_result unhexify(std::string_view input, std::span<std::byte> output) {
    auto input_it = input.begin();
    if ((input.size() % 2 == 1) || (input.size() / 2 > output.size())) {
        return {input_it, std::errc::value_too_large};
    }

    auto output_it = reinterpret_cast<std::uint8_t*>(output.data());
    while (input_it != input.end()) {
        if (auto [_, err] = std::from_chars(input_it, input_it + 2, *output_it, 16);
            err != std::errc{}) {
            return {input_it, err};
        }
        input_it += 2;
        output_it++;
    }

    return {input_it, std::errc{}};
}

constexpr std::to_chars_result hexify(std::span<const std::byte> input, std::span<char> output) {
    auto it = output.begin();
    if ((output.size() % 2 == 1) || (output.size() / 2 < input.size())) {
        return {it.base(), std::errc::value_too_large};
    }

    for (auto i : input) {
        auto end = it + 2;
        if (i < std::byte{0x10}) {
            *it++ = '0';
        }
        if (auto [_, err] = std::to_chars(it.base(), end.base(), satext::to_underlying_type(i), 16);
            err != std::errc{}) {
            return {it.base(), err};
        }
        it = end;
    }

    return {it.base(), std::errc{}};
}

} // namespace satext
